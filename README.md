# Installation
1.
```bash
cp config-sample.js config.js
```
2. Create standalone VK app, get service key and paste it to config.js.
3. Create db, user and paste params to config.js.
4.
```bash
npm i
```

# Run
```bash
node stream.js
```

P.S.: `kupisev-streaming` - linux service.
