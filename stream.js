const config = require('./config');
const vkflow = require('vkflow');
const mysql = require('mysql');
const logger = require('log4js').getLogger();
const db = mysql.createConnection(config.db);

logger.level = 'debug';
db.connect(init);

function init(err) {
	if (err) {
		logger.error(err);
		return;
	}

	var sql = "INSERT INTO advert (title, description, cost, category_id, contact_name, updated_at, created_at) VALUES ('', ?, 0, 1, '', NOW(), NOW())";

	const stream = vkflow(
		config.vk.service_key,
		config.vk.rules
	);

	db.query('SET NAMES utf8mb4', function(err, result) { if (err) logger.error(err); });

	stream.on('data', function(data) {
		data = JSON.parse(data);
		var value = '';
		var i;
		
		try {
			if (data.code > 100 || data.event.action != 'new'
				|| data.event.event_type != 'post'
				|| data.event.author.id < 0
				|| !data.event.text
				|| data.event.text.length == 0) {
				return;
			}

			if (data.event.attachments) {
				for (i = 0; i < data.event.attachments.length; i++) {
					var attach = data.event.attachments[i];

					if (attach.type == 'photo' && attach.photo.photo_604) {
						value += 'photo:' + attach.photo.photo_604 + "\n";
					}
				}
			}

			value += 'author_id:' + data.event.author.id + "\n";
			value += 'post_id:' + data.event.event_id.post_id + "\n";

			var text = data.event.text;
			text = text.replace(/\<br\>/gi, "\n");
			text = text.replace(/&#?[\w\d]+;/gi, "\n");
			text = text.replace(/[\u1F40-\uFFFF]/g, "");
			text = text.replace(/\s+/g, " ");
			text = text.trim();

			value += "\n" + text + "\n";

			db.query(sql, [[value]], function(err, result) {
				if (err) {
					logger.error(err);
				}
			});
			logger.info('yep!');
		} catch (e) {
			logger.error(e)
		}
	});
}
